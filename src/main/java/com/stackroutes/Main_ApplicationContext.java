package com.stackroutes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.stackroute.domain.Actor;
import com.stackroute.domain.Movie;


public class Main_ApplicationContext {

	public static void main(String[] args) {
		ApplicationContext context= new ClassPathXmlApplicationContext("beans.xml");
		Actor actor=context.getBean("Actor", Actor.class);
		actor.displayActorDetails();
		System.out.println("****************************************************");
		System.out.println("printing movie class");
		Movie movie=context.getBean("Movie", Movie.class);
		movie.displayActorDetails();
	}

}
