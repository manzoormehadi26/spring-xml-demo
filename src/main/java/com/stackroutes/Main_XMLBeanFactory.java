package com.stackroutes;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

import com.stackroute.domain.Actor;
import com.stackroute.domain.Movie;

public class Main_XMLBeanFactory {

	public static void main(String[] args) {
		BeanFactory factory = new XmlBeanFactory(new ClassPathResource("beans.xml"));
		Actor actor=factory.getBean("Actor", Actor.class);
		actor.displayActorDetails();
		System.out.println("***************************************");
		System.out.println(" printing movie class");
		Movie movie=factory.getBean("Movie", Movie.class);
		movie.displayActorDetails();
	}

}
