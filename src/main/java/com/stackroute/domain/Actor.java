package com.stackroute.domain;

public class Actor {

	String Name;
	String Gender;
	int Age;
	public void setName(String name) {
		Name = name;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public void setAge(int age) {
		Age = age;
	}
	
	public void displayActorDetails() {
		System.out.println(" Name of an actor is "+Name+"\n Gender of an actor is "+Gender+"\n Age of an actor is "+Age);
	}
}
